using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemSlot
{
    Helmet,
    Necklace,
    Shoulderpad,
    Chestplate,
    Belt,
    Leggings,
    Boots,
    Ring,
    Gloves
}

public class Hero : ManaUser
{
    public int level = 1;
    public int strength, agility, intelligence, points;
    public float xp;
    public const int pointsPerLevel = 5;
    public const float healthPerLevel = 20;
    public const float manaPerLevel = 5;
    public Dictionary<ItemSlot, Item> equippedItems = new Dictionary<ItemSlot, Item>();

    public void Start()
    {
        // doar pentru testing
        AddExperience(1400);
        spells.Add(new SpellBlink("Blink", 5, 5, 40, 2, 6000));
    }

    public new void FixedUpdate()
    {
        base.FixedUpdate();
    }

        public void AddExperience(float amount)
    {
        xp += amount;
        while (xp >= ExperienceForLevel(level))
        {
            LevelUp();
        }
    }

    public float ExperienceForLevel(int lvl)
    {
        return lvl * lvl * 100;
    }

    public void LevelUp()
    {
        ++level;
        points += pointsPerLevel;
        healthMax += healthPerLevel;
        manaMax += manaPerLevel;
        health = healthMax;
        mana = manaMax;
        Debug.Log("Character has reached level " + level);
    }

    public void AddStrength()
    {
        if (points > 0)
        {
            --points;
            ++strength;
        }
    }

    public void AddAgility()
    {
        if (points > 0)
        {
            --points;
            ++agility;
        }
    }

    public void AddIntelligence()
    {
        if (points > 0)
        {
            --points;
            ++intelligence;
        }
    }
}
