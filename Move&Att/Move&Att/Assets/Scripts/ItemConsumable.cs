﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemConsumable : Item
{
    public override void Use(Hero h, int slot)
    {
        --stackSize;
        if (stackSize <= 0)
            RemoveItem(h, slot);
    }
}