using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaUser : Unit
{
    public float mana = 100, manaMax = 100, regen = 1;
    public List<Spell> spells = new List<Spell>();
    public Vector3 castPoint;

    public new void FixedUpdate()
    {
        base.FixedUpdate();
        if (mana < manaMax)
        {
            mana += regen * Time.deltaTime;
            if (mana > manaMax)
                mana = manaMax;
        }
        for (int i = 0; i < spells.Count; ++i)
        {
            Spell spell = spells[i];
            spell.FixedUpdate();
            if (spells.Count <= i)
                break;
            if (!spell.Equals(spells[i]))
            {
                --i;
            }
        }
    }

    public void AddMana(float amount)
    {
        mana += amount;
        if (mana > manaMax)
            mana = manaMax;
        else if (mana < 0)
            mana = 0;
    }

    public bool SpendMana(float amount)
    {
        if (mana < amount)
            return false;
        mana -= amount;
        return true;
    }

    public bool HasMana(int index)
    {
        return spells[index].manaCost <= mana;
    }

    public bool IsReady(int index)
    {
        return spells[index].cooldown == 0;
    }

    // 0 poate fi click dreapta, 1, 2, 3, 4 etc pot fi pe tastele 1, 2, 3, 4
    public bool CastSpell(int index)
    {
        if (spells[index].Cast(this, castPoint))
            return true;
        return false;
    }
}
