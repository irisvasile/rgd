﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEquipable : Item
{
    // slot pentru fiecare tip de item
    int equipSlot;

    public override void Use(Hero h, int slot)
    {
        MoveItem(h, slot, equipSlot);
    }
}
