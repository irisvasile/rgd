﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item
{
    public string itemName;
    public int stackSize, stackSizeMax;

    public void UseItem(Hero h, int slot)
    {
        Use(h, slot);
    }

    public static void RemoveItem(Hero h, int slot)
    {
        // remove item in slot
    }

    public static void MoveItem(Hero h, int slot1, int slot2)
    {
        // get item 1 from slot 1
        // get item 2 from slot 2
        RemoveItem(h, slot1);
        RemoveItem(h, slot2);
        // put item 1 in slot 2
        // put item 2 in slot 1
    }

    public abstract void Use(Hero h, int slot);
}
